# Surfstopp

## Internet Blocker

This is a prototype for disabling web-access during exams and certifications.

![Cisco Meraki](https://gitlab.com/pascal.cantaluppi/surfstopp/raw/deploy/public/meraki.png)

```
Cisco Meraki Web API Application Access (Proof of Concept)
```

Install node.js and clone this git git repository to run the Project.

Execute the following commands to install / test / run the app.


### Build

### `npm install`

### Test

### `npm test`

### Run

### `npm start`
