import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

import Classroom from "./Classroom";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  }
}));

export default function HiddenGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={6} lg={4} xl={3}>
          <Classroom />
        </Grid>
      </Grid>
    </div>
  );
}
