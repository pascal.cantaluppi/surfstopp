import React from "react";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import meraki from "../Modules/Meraki";

export default function SwitchLabels() {
  const [state, setState] = React.useState({
    checked: true
  });

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
    meraki.api(event.target.checked);
  };

  return (
    <FormGroup row>
      <FormControlLabel
        control={
          <Switch
            checked={state.checked}
            onChange={handleChange("checked")}
            value="checked"
            color="primary"
          />
        }
        label="Internet"
      />
    </FormGroup>
  );
}
