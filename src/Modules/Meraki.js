var axios = require("axios");
require("dotenv").config();

const options = {
  headers: {
    "X-Cisco-Meraki-API-Key": process.env.API_KEY,
    "Content-Type": "application/json"
  }
};

export default {
  api: function(turnOn) {
    axios
      .put(
        "https://n176.meraki.com/api/v0/devices/Q2BX-9JLP-JM89/switchPorts/10",
        {
          enabled: turnOn
        },
        options
      )
      .then(
        response => {
          console.log(response.data.enabled);
        },
        error => {
          console.log(error);
        }
      );
  }
};
