import React from "react";
import NavBar from "./Components/NavBar";
import HiddenGrid from "./Components/HiddenGrid";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <HiddenGrid />
        <br />
        <br />
      </div>
    );
  }
}

export default App;
