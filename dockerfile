# build environment
FROM node:12.14.0-alpine3.10 as builder
WORKDIR /app
COPY . .
RUN npm i
RUN npm run build

# production environment
FROM node:12.14.0-alpine3.10
RUN npm i -g serve
WORKDIR /app
COPY --from=builder /app/build .
CMD ["serve", "-p", "5001", "-s", "."]